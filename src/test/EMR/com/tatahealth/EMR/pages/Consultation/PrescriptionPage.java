package com.tatahealth.EMR.pages.Consultation;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PrescriptionPage {

	public WebElement getDrugName(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='drugName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement selectMedicineFromDropDown(String value,Integer row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);

		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	/*
	public WebElement getDoseField(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='dosageValue"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDoseUnitField(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='dosageUnit"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	
	public WebElement getFrequency(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='frequency"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSelectFrequencyFromDropDown(String value,int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='frequency"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(element, "Click frequency text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to frequency field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
	
	public WebElement getDuration(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='duration"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDurationUnit(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='durationUnit"+row+"']";
		//select[@id='durationUnit0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDosageAdvice(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='dosageAdvice"+row+"']";
		//select[@id='dosageAdvice0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFrequencyInstruction(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='freqSpecInstruction"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddMedicineButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='add-medicine-btn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement saveMedicines(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getTDHCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='1']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCIMSCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='2']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getClinicSpecificCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='3']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMedicinMasterFromDropDown(Integer row,String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);
		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a/b";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		 
				
		return element;
	}
	
	public WebElement getFirstMedicineFromDropDown(Integer row,String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);
		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		 
				
		return element;
	}
	
	public WebElement getSelectRegularlyDurationFromDropDown(String value,int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='duration"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(element, "Click duration text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to duration field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
	
	public WebElement getInteractionDialogBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='interactionMedicineDialogBox']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNOInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		//String xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons no-titlebar']//button[@class='ui-button ui-corner-all ui-widget'][contains(text(),'No')]";
		String xpath = "//button[contains(text(),'No')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeleteRow(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//tr[@id='"+row+"']/th[@id='actionBlock']/button";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getYesInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@aria-describedby='interactionMedicineDialogBox']/div[3]/div/button[1]";
	//	String xpath = "//button[contains(text(),'Yes')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getStartDate(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//input[contains(@class,'startDate"+row+"')]";
		//System.out.println("date row : "+ xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//td[contains(@class,'ui-datepicker-current-day ui-datepicker-today')]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFutureDate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//td[@data-handler='selectDay']";
		WebElement element = null;
		List<WebElement> tdElements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		if(tdElements.size()>1) {
			xpath = "//td[@data-handler='selectDay'][2]";
		}else {
			xpath = "//a[@data-handler='next']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			Web_GeneralFunctions.click(element, "Click Next button in start date picker", driver, logger);
			Thread.sleep(1000);
			xpath = "//td[@data-handler='selectDay'][1]";
		}
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
	//	String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/span";
		String xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons']/div/span";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNOButtonInDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/div/button[contains(text(),'No')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getYesButtonInDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/div/button[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCIMSLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Prescription']//label[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getClinicSpecificLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Prescription']//label[3]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTDHLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//body/div[@id='wrapper']/div[@class='pagepad pagepad-innerpage']/div[@id='pageContent']/div[@id='consultationMainContainer']/div[@class='innerpagepad']/div[@id='rootwizard']/div[@class='tab-content new-design-tab-content']/div[@id='consultation_tab_container']/div[@id='containerOfPrescription']/div[@id='containerFordiagnosisAndPrescription']/div[@id='prescription']/div[@id='Prescription']/div[@id='prescriptionMasterFormContainer']/label[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getPrescriptionLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//body/div[@id='wrapper']/div[@class='pagepad pagepad-innerpage']/div[@id='pageContent']/div[@id='consultationMainContainer']/div[@class='innerpagepad']/div[@id='rootwizard']/div[@class='tab-content new-design-tab-content']/div[@id='consultation_tab_container']/div[@id='containerOfPrescription']/div[@id='containerFordiagnosisAndPrescription']/div[@id='prescription']/div[@id='Prescription']/div[@id='prescriptionMasterFormContainer']/h4[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddTemplateButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='addTemplate']";
		//button[@id='addTemplate']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateNameModal']//button[@id='submitTemplateForm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplateNameField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='userDefinedTemplateName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCloseTemplateButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateNameModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplateFromDropDown(String value,WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getTemplateName(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions.click(element, "Click template name text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to template name field", driver, logger);

		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click medicine text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}*/
	
	public WebElement getDoseField(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='dosageValue"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDoseUnitField(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='dosageUnit"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFrequency(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='frequency"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	// new xpaths --Sonalin
	
public WebElement getLastDrugName(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//textarea[contains(@id,'drugName')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
   public WebElement getLastFrequency(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//input[contains(@id,'frequency')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   public WebElement getSelectLastFrequencyFromDropDown(String value,int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//input[contains(@id,'frequency')])[last()]";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click frequency text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to frequency field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
   public WebElement getLastDoseField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//input[contains(@id,'dosageValue')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getLastDoseUnitField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//select[contains(@id,'dosageUnit')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
   
   public WebElement getLastDuration(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//input[contains(@id,'duration')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getLastDurationUnit(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//input[contains(@id,'duration')])[last()]";
		//select[@id='durationUnit0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
		// (//select[contains(@id,'duration')])[last()] --duration dropdown
	}
	
	public WebElement getLastDosageAdvice(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//select[contains(@id,'dosageAdvice')])[last()]";
		//select[@id='dosageAdvice0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getLastFrequencyInstruction(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//textarea[contains(@id,'freqSpecInstruction')])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
public WebElement getLastStartDate(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "(//input[contains(@class,'startDate')])[last()]";
		//System.out.println("date row : "+ xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	 
   //....................end .....................................
   
	public WebElement getSelectFrequencyFromDropDown(String value,int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='frequency"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click frequency text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to frequency field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
	
	public WebElement getDuration(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='duration"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDurationUnit(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='durationUnit"+row+"']";
		//select[@id='durationUnit0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDosageAdvice(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='dosageAdvice"+row+"']";
		//select[@id='dosageAdvice0']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFrequencyInstruction(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='freqSpecInstruction"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddMedicineButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='add-medicine-btn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement saveMedicines(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='Print']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getTDHCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='1']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCIMSCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='2']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getClinicSpecificCheckBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@class='medicineMasterPrescription'][@value='3']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMedicinMasterFromDropDown(Integer row,String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click medicine text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a/b";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		 
			System.out.println("element..."+ element );	
		return element;
	}
	
	public WebElement getFirstMedicineFromDropDown(Integer row,String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click medicine text field", driver, logger);
		Thread.sleep(1000);
		
		generalFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		 
				
		return element;
	}
	
	public WebElement getSelectRegularlyDurationFromDropDown(String value,int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='duration"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click duration text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to duration field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
		
	}
	
	public WebElement getInteractionDialogBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='interactionMedicineDialogBox']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNOInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		//String xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons no-titlebar']//button[@class='ui-button ui-corner-all ui-widget'][contains(text(),'No')]";
		String xpath = "//button[@class='ui-button ui-corner-all ui-widget'][contains(text(),'No')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeleteRow(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//tr[@id='"+row+"']/th[@id='actionBlock']/button";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
public WebElement getLastDeleteRow(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//span[@class='glyphicon glyphicon-remove btnDelete_def gray-icon'])[last()]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}

	

	public WebElement getYesInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='ui-button ui-corner-all ui-widget'][contains(text(),'Yes')]";
	//	String xpath = "//button[contains(text(),'Yes')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getStartDate(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//input[contains(@class,'startDate"+row+"')]";
		//System.out.println("date row : "+ xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//td[contains(@class,'ui-datepicker-current-day ui-datepicker-today')]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getFutureDate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//td[@data-handler='selectDay']";
		WebElement element = null;
		List<WebElement> tdElements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		if(tdElements.size()>1) {
			xpath = "//td[@data-handler='selectDay'][2]";
		}else {
			xpath = "//a[@data-handler='next']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
			generalFunctions.click(element, "Click Next button in start date picker", driver, logger);
			Thread.sleep(1000);
			xpath = "//td[@data-handler='selectDay'][1]";
		}
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
	String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/span";
		//String xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons']/div/span";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getNOButtonInDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/div/button[contains(text(),'No')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getYesButtonInDuplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/div/button[contains(text(),'Yes')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCIMSLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Prescription']//label[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getClinicSpecificLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='Prescription']//label[3]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTDHLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//body/div[@id='wrapper']/div[@class='pagepad pagepad-innerpage']/div[@id='pageContent']/div[@id='consultationMainContainer']/div[@class='innerpagepad']/div[@id='rootwizard']/div[@class='tab-content new-design-tab-content']/div[@id='consultation_tab_container']/div[@id='containerOfPrescription']/div[@id='containerFordiagnosisAndPrescription']/div[@id='prescription']/div[@id='Prescription']/div[@id='prescriptionMasterFormContainer']/label[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getPrescriptionLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//body/div[@id='wrapper']/div[@class='pagepad pagepad-innerpage']/div[@id='pageContent']/div[@id='consultationMainContainer']/div[@class='innerpagepad']/div[@id='rootwizard']/div[@class='tab-content new-design-tab-content']/div[@id='consultation_tab_container']/div[@id='containerOfPrescription']/div[@id='containerFordiagnosisAndPrescription']/div[@id='prescription']/div[@id='Prescription']/div[@id='prescriptionMasterFormContainer']/h4[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAddTemplateButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='addTemplate']";
		//button[@id='addTemplate']
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateNameModal']//button[@id='submitTemplateForm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplateNameField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='userDefinedTemplateName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCloseTemplateButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateNameModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getTemplateFromDropDown(String value,WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click medicine text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to diagnosis field", driver, logger);
		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getTemplateName(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='templateSearchField']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click template name text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to template name field", driver, logger);

		Thread.sleep(1000);
		
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		
		//System.out.println("xpath : "+xpath);
		
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
				
		return element;
	}
	
	public List<String> getPrescriptionData(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		List<String> li = new ArrayList<String>();
		
		
		String attribute = "";
		String value = "";
		Integer i = 0;
		Integer number = 1;
		String xpath = "//table[@id='medicine_table']/tbody/tr";
		List<WebElement> medicineTable = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = medicineTable.size();
		WebElement element = null;
		
		while(i<size-1) {
			xpath = "//tr[@id='drugGeneric"+i+"']";
			if(Web_GeneralFunctions.isDisplayed(xpath, driver)) {
				xpath = "//tr[@id='drugGeneric"+i+"']/td/b";
				element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
				attribute += " "+Web_GeneralFunctions.getText(element, "get drug composition", driver, logger);
				Thread.sleep(1000);
			}else {
				attribute += number.toString()+" ";
				
					value = Web_GeneralFunctions.getAttribute(getDrugName(i, driver, logger), "value", "Get drug name", driver, logger);
					
					attribute += value;
					Thread.sleep(1000);
					value = Web_GeneralFunctions.getAttribute(getDoseField(i, driver, logger), "value", "Get drug name", driver, logger);
					if(!value.isEmpty() && value != null) {
						attribute += " "+value;
						attribute += " "+Web_GeneralFunctions.getAttribute(getDoseUnitField(i, driver, logger), "value", "Get drug name", driver, logger);
						Thread.sleep(1000);
					}
					
					attribute += " "+Web_GeneralFunctions.getAttribute(getFrequency(i, driver, logger), "value", "Get drug name", driver, logger);
					Thread.sleep(1000);
				
					value = Web_GeneralFunctions.getAttribute(getDuration(i, driver, logger), "value", "Get drug name", driver, logger);
					Thread.sleep(1000);
					
					if(!value.equalsIgnoreCase("Regularly")) {
						attribute += " "+value;
						attribute += " "+Web_GeneralFunctions.getAttribute(getDurationUnit(i, driver, logger), "value", "Get drug name", driver, logger);
						Thread.sleep(1000);
					}else{
						attribute += " "+value;
					}
					
					value = Web_GeneralFunctions.getAttribute(getDosageAdvice(i, driver, logger), "value", "Get drug name", driver, logger);
					if(value.equalsIgnoreCase("af")) {
						value = "After Food";
					}else if(value.equalsIgnoreCase("bf")) {
						value = "Before Food";
					}
					attribute += " "+value;
					Thread.sleep(1000);
					
					attribute += " "+Web_GeneralFunctions.getAttribute(getStartDate(i, driver, logger), "value", "Get drug name", driver, logger);
					Thread.sleep(1000);
					
					value = Web_GeneralFunctions.getAttribute(getFrequencyInstruction(i, driver, logger), "value", "Get drug name", driver, logger);
					Thread.sleep(1000);
					if(!value.isEmpty()) {
						attribute += " "+value;
					}else {
						attribute += " -";
					}
			}
			
			i++;
			number++;
			
			System.out.println("attribute : "+attribute);
		}
		li.add(attribute);
		return li;
	}
	
	public boolean duplicateBox(WebDriver driver,ExtentTest logger)throws Exception{
		
		//String xpath = "//div[@aria-describedby='duplicateMedicineDialogBox']/div/span";
		//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons']/div/span
		String xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog-buttons']/div/span";
		boolean element = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return element;
	}
	
	
	public WebElement getRepeatPrescription(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='checkRepeatPrescription']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getRepeatPrescriptionSweetAlertMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//p[contains(text(),'Do you want to add drugs from old prescription?')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getRepeatPrescriptionNo(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='cancel']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getRepeatPrescriptionYes(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='confirm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
}
