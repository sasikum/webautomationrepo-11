package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.apache.commons.lang.math.RandomUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PrescriptionTemplatePages {
	
	
	
	/*
	 * create template button
	 */
	public WebElement getCreateTemplate(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='create_tmp']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	/*
	 * template search field
	 */
	public WebElement getTemplateSearchField(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='templateSearchField']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	/*
	 * create template name field
	 */
	public WebElement getTemplateNameField(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='template-name-field-container']//input[@id='userDefinedTemplateName']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	/*
	 * save template button
	 */
	public WebElement getSaveTemplateButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='templateSaveBtn']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	/*
	 * update template button
	 */
	public WebElement getUpdateTemplateButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[contains(text(),'Update Template')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	/*
	 * delete template button
	 */
	public WebElement getDeleteTemplateButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='btn btn-danger btnmarginleft']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getDeleteRow(WebDriver driver,ExtentTest logger){
		
		String xpath = "//th[@id='actionBlock']/button";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getSaveTemplateWithEmptyDataMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter template name')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getSaveTemplateWithEmptyMedicineDataMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter at least one medicine')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDeleteRowWithEmptyDataMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter prescription')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	

		
		
	
	
	/*
	 * create template name field
	 */
	public WebElement getPrescriptionMenu(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[contains(text(),'Prescription')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getId(WebDriver driver,ExtentTest logger){
		
		String xpath = "//table[@id='medicine_table']/tbody/tr";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);

		int index = li.size();
		xpath = "//table[@id='medicine_table']/tbody/tr["+index+"]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement select1stMedicineFromDropDown(String row,WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String drugNameField = "//textarea[@id='drugName"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(drugNameField, driver, logger);

		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);

		
		Web_GeneralFunctions.click(element, "Click medicine text field", driver, logger);

		Thread.sleep(1000);
	
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]";
		

		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getYesInDeleteTemplatePopUp(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@aria-describedby='templateDeleteDialogBox']/div[3]/div/button[1]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement selectMedicineFromDropDown(int row,String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//textarea[@id='drugName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions.click(element, "click drug name", driver, logger);
		Thread.sleep(1000);
		
		Web_GeneralFunctions.sendkeys(element, value, "sending values in drug name field", driver, logger);
		Thread.sleep(1000);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[2]";
		
		 
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		Thread.sleep(2000);
		int size = li.size();
		
		int index =RandomUtils.nextInt(size);
		if(index==0) {
			index+=1;
		}
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Thread.sleep(1000);
		
		return element;
		
	}
	
	public WebElement getFrequencyLabel(WebDriver driver,ExtentTest logger)  {
		
		 return(Web_GeneralFunctions.findElementbyXPath("//div[@id='main-form-fields-continer']//child::th[contains(text(),'Frequency')]", driver, logger));
		
		
	}
	
	public WebElement getPrescriptionSectionHeader(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@id='prescriptionTemplate']//h4[contains(text(),'Prescription')])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getTDHCatalogLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@class='medicineMasterPrescription'][@value='1']//parent::label";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	

	public WebElement getCIMSLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@class='medicineMasterPrescription'][@value='2']//parent::label";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getClinicMasterLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@class='medicineMasterPrescription'][@value='3']//parent::label";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMasterAdministrationLinkInLeftNav(WebDriver driver,ExtentTest logger) {
		String xpath ="//li[@name='masterAdministration']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getConfigurationMasterInMasterAdministration(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[contains(text(),'Configuration Master')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicineMasterEditlink(WebDriver driver,ExtentTest logger) {
		String xpath ="//td[contains(text(),'Medicine Master')]//parent::tr//td/a";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getTDHMasterCheckboxInConfigurationMaster(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),'Parameter value')]//parent::div//input[@id='paramValue1']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCIMSMasterCheckboxInConfigurationMaster(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),'Parameter value')]//parent::div//input[@id='paramValue2']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getClinicSpecificMasterCheckboxInConfigurationMaster(WebDriver driver,ExtentTest logger) {
		String xpath ="//label[contains(text(),'Parameter value')]//parent::div//input[@id='paramValue3']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSaveButtonInConfigurationMaster(WebDriver driver,ExtentTest logger) {
		String xpath ="(//button[contains(text(),'Save')])[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllMedicineCategorySuggestionWebElementsAsList(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a/b";
		return (Web_GeneralFunctions.listOfElementsbyXpath(xpath, driver, logger));
	}
	
	public List<String> getAllMedicineCategorySuggestionNamesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> medicineCategoryList = new ArrayList<>();
		List<WebElement> medicineSuggestions = getAllMedicineCategorySuggestionWebElementsAsList(driver,logger);
		for(WebElement e: medicineSuggestions) {
			medicineCategoryList.add(Web_GeneralFunctions.getText(e, "getting medicine category text", driver, logger));
		}
		return medicineCategoryList;
	}
	
	public List<WebElement> getAllSavedTemplatesAsList(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public WebElement getAddMedicineButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[@id='add-medicine-btn']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicineTable(WebDriver driver,ExtentTest logger) {
		String xpath = "//table[@id='medicine_table']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicineSearchField(WebDriver driver,ExtentTest logger) {
		String xpath ="//textarea[@name='medicineSearchField']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getAllMedicineElementsAsList(WebDriver driver, ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<String> getAllMedicinesNamesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> medicineNamesList = new ArrayList<>();
		List<WebElement> allMedicineElements = getAllMedicineElementsAsList(driver,logger);
		for(WebElement e :allMedicineElements) {
			medicineNamesList.add(Web_GeneralFunctions.getText(e, "extracting medicine names", driver, logger));
		}
		return medicineNamesList;
	}
	
	public List<WebElement> getAllFrequencySuggestionsAsWebElementsAsList(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<String> getAllFrequenciesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> frequencyNamesList = new ArrayList<>();
		List<WebElement> allFrequencyElements = getAllFrequencySuggestionsAsWebElementsAsList(driver,logger);
		for(WebElement e :allFrequencyElements) {
			frequencyNamesList.add(Web_GeneralFunctions.getText(e, "extracting frequency names", driver, logger));
		}
		return frequencyNamesList;
	}
	
	public List<String> getDefaultFrequencyList(){
		return(Arrays.asList(new String[] {"0-0-1","0-1-0","1-0-0","1-1-0","1-0-1","0-1-1","1-1-1","1-1-1-1","SOS","Stat"}));
	}
	
	public List<WebElement> getAllDurationSuggestionsWebElementsAsList(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<String> getAllDurationSuggestinsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> durationNamesList = new ArrayList<>();
		List<WebElement> durationElements = getAllFrequencySuggestionsAsWebElementsAsList(driver,logger);
		for(WebElement e :durationElements) {
			durationNamesList.add(Web_GeneralFunctions.getText(e, "extracting duration names", driver, logger));
		}
		return durationNamesList;
	}
	
	public WebElement getRegularlyDurationElement(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
public WebElement getDaysDurationUnit(int row,WebDriver driver,ExtentTest logger){
		
		String xpath = "//select[@id='durationUnit"+row+"']//option[@value='Day(s)']";
		//select[@id='durationUnit0']
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
public List<WebElement> getColumnsListInMedicineTable(WebDriver driver,ExtentTest logger) {
	String xpath ="//table[@id='medicine_table']//thead//th";
	return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
}

public WebElement getMLDoseUnit(int row,WebDriver driver,ExtentTest logger){
	
	String xpath = "//select[@id='dosageUnit"+row+"']//option[@value='ml']";
	//select[@id='durationUnit0']
	return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
}

public WebElement getLitresDoseUnit(int row,WebDriver driver,ExtentTest logger){
	
	String xpath = "//select[@id='dosageUnit"+row+"']//option[@value='Units']";
	//select[@id='durationUnit0']
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}

public WebElement getSelectDoseUnit(WebDriver driver,ExtentTest logger){
	
	String xpath = "//select[@id='dosageUnit0']//option[contains(text(),'Select')]";
	//select[@id='durationUnit0']
	return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
}

public List<String> getAllDoseUnitsAsList(int row,WebDriver driver,ExtentTest logger) throws Exception {
	String xpath = "//select[@id='dosageUnit"+row+"']//option";
	List<String> doseUnitsAsList = new ArrayList<>();
	List<WebElement> doseUnitElements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	for(WebElement e:doseUnitElements) {
		doseUnitsAsList.add(Web_GeneralFunctions.getText(e, "extract text from options", driver, logger));
	}
	return doseUnitsAsList;

}

public List<String> getExpectedDoseUnitsAsList() {
	return(Arrays.asList(new String[] {"Select","ml","Litre","mg","Units","Drops","ampoule(s)","respoule(s)","sachet(s)","puff(s)","spray(s)"}));
	
}

public List<String> getAllAdviceUnitsAsList(int row,WebDriver driver,ExtentTest logger) throws Exception {
	String xpath = "//select[@id='dosageAdvice"+row+"']//option";
	List<String> adviceUnitsAsList = new ArrayList<>();
	List<WebElement> adviceUnitElements = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	for(WebElement e:adviceUnitElements) {
		adviceUnitsAsList.add(Web_GeneralFunctions.getText(e, "extract text from options", driver, logger));
	}
	return adviceUnitsAsList;

}

public List<String> getExpectedAdviceUnitsAsList() {
	return(Arrays.asList(new String[] {"NA","After Food","Before Food","With Food","Empty Stomach","Before Sleeping","External application"}));
	
}

public WebElement getAfterFoodAdviceUnitOption(int row,WebDriver driver,ExtentTest logger) {
	String xpath ="//select[@id='dosageAdvice"+row+"']//option[@value='After Food']";
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}

public WebElement getAdviceUnitDropDownElement(int row,WebDriver driver,ExtentTest logger) {
	String xpath ="//select[@id='dosageAdvice"+row+"']";
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}

public WebElement getEmptyStomachAdviceUnitOption(int row,WebDriver driver,ExtentTest logger) {
	String xpath ="//select[@id='dosageAdvice"+row+"']//option[@value='Empty Stomach']";
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}
public WebElement getInstructionFieldTextBox(int row,WebDriver driver,ExtentTest logger) {
	String xpath ="//textarea[@id='freqSpecInstruction"+row+"']";
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}

public List<WebElement> getMedicineTableRowsList(WebDriver driver,ExtentTest logger){
	String xpath ="//table[@id='medicine_table']//tbody//tr";
	return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
}

public WebElement getMedicineComposition(int row,WebDriver driver,ExtentTest logger) {
	String xpath ="//table[@id='medicine_table']//tr[@id='drugGeneric"+row+"']//td[@id='drugComposition"+row+"']";
	return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
}

public List<String> getAllMedicinesNamesInMedicineTableAsList(WebDriver driver,ExtentTest logger) throws Exception {
	String xpath ="//table[@id='medicine_table']//tbody//tr//textarea[@placeholder='Search Medicine']";
List<WebElement> allMedicineRows = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
List<String> medicineNames = new ArrayList<>();
for (WebElement e :allMedicineRows) {
	medicineNames.add(Web_GeneralFunctions.getText(e, "extract medicine text", driver, logger));
	}
return medicineNames;
}

public List<WebElement> getAllCloseElementsOfMedicineTableInConsultationList(WebDriver driver,ExtentTest logger) {
	String xpath ="//table[@id='medicine_table']//tbody//th//button";
 return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));


}

public WebElement getNOInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
	

	String xpath = "//div[@aria-describedby='interactionMedicineDialogBox']//child::button[contains(text(),'No')]";
	return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	
}

public WebElement getYesInInteractionPopUp(WebDriver driver,ExtentTest logger)throws Exception{
	
	String xpath = "//div[@aria-describedby='interactionMedicineDialogBox']//child::button[contains(text(),'Yes')]";

	WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	return element;
}



}
