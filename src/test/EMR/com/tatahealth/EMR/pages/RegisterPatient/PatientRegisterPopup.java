package com.tatahealth.EMR.pages.RegisterPatient;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class PatientRegisterPopup {
	
	
	
	public WebElement getNewPatientSelection(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#newPatientButtonInConfirmPopup",driver,logger);
		return element;	
	}
	
	
	public WebElement getExistingPatientSelection(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("#collapseH > div > table > tbody > tr:nth-child(1) > td.greentext.greenlink > a",driver,logger);
		return element;	
	}
	
	
	public WebElement getConfirmBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button",driver,logger);
		return element;	
	}
	
	
}